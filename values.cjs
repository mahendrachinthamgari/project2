function valuesArray(obj){
    if(Array.isArray(obj)){
        return [];
    }
    const objPropertyArray = [];
    for(property in obj){
        if(obj[property] instanceof Function){
            break;
        }
        objPropertyArray.push(obj[property]);
    }
    return objPropertyArray;
}


module.exports = valuesArray;