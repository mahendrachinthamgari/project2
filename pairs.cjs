function pairs(obj){
    if(Array.isArray(obj)){
        return [];
    }
    const pairsList = [];
    for(property in obj){
        pairsList.push([property, obj[property]]);
    }
    return pairsList;
}


module.exports = pairs;