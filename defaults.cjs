function defaults(obj, defaultProps = {flavor: "vanilla", sprinkles: "lots"}){
    if(obj == null || Array.isArray(obj)){
        return defaultProps;
    }
    const combinedObjects = {};
    for(let property in obj){
        combinedObjects[property] = obj[property];
    }
    for(let property in defaultProps){
        if(!obj.hasOwnProperty(property)){
            combinedObjects[property] = defaultProps[property];
        }
    }
    return combinedObjects;
}



module.exports = defaults;