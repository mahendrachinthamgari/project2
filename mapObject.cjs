function mapObjects(obj, cb){
    if(obj == null || Array.isArray(obj)){
        return {};
    }
    const cbResult = cb(obj);
    for(let property in obj){
        obj[property] += cbResult;
    }
    return obj;
}

module.exports = mapObjects