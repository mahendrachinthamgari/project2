function keys(obj){
    if(Array.isArray(obj)){
        return [];
    }
    const keysArray = [];
    for(let property in obj){
        keysArray.push(property);
    }
    return keysArray;
}


module.exports = keys;