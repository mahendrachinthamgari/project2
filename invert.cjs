function invert(obj){
    if(Array.isArray(obj)){
        return {};
    }
    const reverseObj = {};
    for(let property in obj){
        reverseObj[obj[property]] = property;
    }
    return reverseObj;
}


module.exports = invert;